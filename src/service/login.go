package service

import (
	"fmt"
	"github.com/tebeka/selenium"
	"mcncomment/src/util"
	"os"
	"strconv"
)

func Login() {

	err := error(nil)

	//加载基础配置（浏览器驱动）
	configFields := util.ListConfigFields()
	chromeDriverPath = configFields[0]
	port, err = strconv.Atoi(configFields[1])
	if err != nil {
		panic(err)
	}

	// Start a WebDriver server instance
	opts := []selenium.ServiceOption{
		selenium.Output(os.Stderr), // Output debug information to STDERR.
	}
	selenium.SetDebug(false)
	service, _ := selenium.NewChromeDriverService(chromeDriverPath, port, opts...)
	defer service.Stop()

	// Connect to the WebDriver instance running locally.
	caps := selenium.Capabilities{"browserName": "chrome"}
	wd, err := selenium.NewRemote(caps, fmt.Sprintf("http://localhost:%d/wd/hub", port))
	if err != nil {
		panic(err)
	}

	//打开抖音官网
	err = wd.Get("https://www.douyin.com/")
	if err != nil {
		panic(err)
	}

	//扫描登录成功后，在终端窗口里按回车，结束登录流程
	a := ""
	fmt.Scanln(&a)

	//保存token
	cookies, _ := wd.GetCookies()
	util.SaveCookies(cookies)

	defer wd.Quit()
}
