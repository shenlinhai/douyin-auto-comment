package util

import (
	"io/ioutil"
)

func writeCookiesFile(data []byte) {
	err := ioutil.WriteFile("./cookies", data, 0766)
	if err != nil {
		panic(err)
	}
}

func readCookiesFile() []byte {
	data, err := ioutil.ReadFile("./cookies")
	if err != nil {
		panic(err)
	}
	return data
}

func readConfigFile() []byte {
	data, err := ioutil.ReadFile("./config.txt")
	if err != nil {
		panic(err)
	}
	return data
}

func readCommentsFile() []byte {
	data, err := ioutil.ReadFile("./comments.txt")
	if err != nil {
		panic(err)
	}
	return data
}
