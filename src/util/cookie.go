package util

import (
	"encoding/json"
	"github.com/tebeka/selenium"
)

func ListCookies() []selenium.Cookie {
	//读取cookies
	cookiesBytes := readCookiesFile()
	var cookies []selenium.Cookie
	err := json.Unmarshal(cookiesBytes, &cookies)
	if err != nil {
		panic(err)
	}
	return cookies
}

func SaveCookies(cookies []selenium.Cookie) {
	//保存cookies
	cookiesBytes, _ := json.Marshal(cookies)
	writeCookiesFile(cookiesBytes)
}
