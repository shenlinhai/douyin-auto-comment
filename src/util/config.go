package util

import "strings"

func ListConfigFields() []string {
	configBytes := readConfigFile()
	var config = string(configBytes)
	config = strings.Replace(config, "\r", "", -1)
	return strings.Split(config, "\n")
}

func ListComments() []string {
	commentsBytes := readCommentsFile()
	var comments = string(commentsBytes)
	comments = strings.Replace(comments, "\r", "", -1)
	return strings.Split(comments, "\n")
}
