# douyin-auto-comment
## 基于 Go + Selenium 的抖音自动评论脚本
### 待完善，仅供学习使用

***

### 前置工作

#### 下载`chrome`浏览器和`chromedriver.exe`驱动文件（两者版本要对上）。
#### 将`chromedriver.exe`驱动文件放入项目`bin`目录下。
#### 填写`comments.txt`（评论内容集合，按换行符分割）。
#### 填写`config.txt`（基础参数配置，按换行符分割，第一行-浏览器驱动所处路径、第二行-启动端口号、第三行-直播间链接，第四行-自动回复周期`秒`）。

***

### windows启动程序
#### 一个标准的bin目录应该有如下文件
* bin
  * login.exe `抖音登录脚本`
  * comment.exe `抖音自动评论脚本`
  * comments.txt `评论集合`
  * config.txt `基础配置`
  * chromedriver.exe `浏览器驱动文件`

#### 先启动`login.exe`，会自动打开浏览器与一个CMD终端，并自动进入抖音首页，然后可进行登录操作。
#### 登录完成后，先不要关闭浏览器，在CMD终端里按一下回车，此时将会保存到抖音cookie至本地（bin目录里会生成一个`cookies`文件），然后程序结束，浏览器自动关闭。
#### 再启动`comment.exe`，会自动打开浏览器与一个CMD终端，并自动进入抖音首页，再自动跳转至指定直播间，然后则开始自动评论工作，关闭浏览器或者CMD终端即可终止程序。